import math

FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'


def is_valid_input(base):
    """
    Validate that base is OK
    :param base:
    :return:
    """
    if base > 36 or base <= 0 or not isinstance(base, int):
        raise ValueError("Base {} is not a valid base".format(base))
    return True


def create_base_list(base):
    """
    This function get base and generate a list of all possible chars
    Return ValueError if input is not valid
    :param base:
    :return:
    """
    if is_valid_input(base):
        all_chars = list(map(lambda x: chr(x), range(ord(FIRST_DIGIT), ord(LAST_CHARACTER) + 1)))
        all_filtered = list(filter(lambda x: ord(x) <= ord(LAST_DIGIT) or ord(x) >= ord(FIRST_CHARACTER), all_chars))
        base_list = all_filtered[:base]
        return base_list


def decimal_to_base(as_decimal, dst_base):
    """
    This function get decimal number and convert it to dst_base base
    :param as_decimal: 
    :param dst_base: 
    :return: 
    """
    if as_decimal is 0:
        return "0"
    base_list = create_base_list(dst_base)
    divider = as_decimal
    all_res_div = []
    while divider:
        remainder = divider % dst_base
        divider = int(divider / dst_base)
        all_res_div.append(base_list[remainder])
    answer = ''.join([str(i) for i in all_res_div[::-1]])
    return answer


def any_to_decimal(num_as_string, src_base):
    """
    This function get number as string and base and convert to base 10 (decimal)
    :param num_as_string: 
    :param src_base: 
    :return: 
    """
    num_as_string = num_as_string.upper()
    base_list = create_base_list(src_base)
    as_decimal = 0
    power = len(num_as_string) - 1
    for i in range(len(num_as_string)):
        if num_as_string[i] not in base_list:
            raise TypeError("The number {} is not legal in base {}".format(num_as_string, src_base))
        num = base_list.index(num_as_string[i])
        as_decimal += math.pow(src_base, power) * num
        power -= 1
    return int(as_decimal)


def convert(num_as_string, src_base, dst_base):
    """
    This function convert num_as_string from src_base to dst_base
    The function return false if num_as_string is not valid in src_base
    :param num_as_string: number as string
    :param src_base: source base
    :param dst_base: destination base
    """
    try:
        as_decimal = any_to_decimal(num_as_string, src_base)
        answer = decimal_to_base(as_decimal, dst_base)
    except Exception as e:
        print(e)
        return
    return answer


def main():
    try:
        number_input = input("Please insert your number\n")
        source_base = int(input("Insert source base\n"))
        destination_base = int(input("Insert destination base\n"))
        result = convert(number_input, source_base, destination_base)
        if result:
            print("Its representation in {} base is {}".format(destination_base, result))
    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()
