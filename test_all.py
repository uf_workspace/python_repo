from main import create_base_list, any_to_decimal, convert
import pytest


def test_create_base_list():
    assert len(create_base_list(2)) == 2
    assert len(create_base_list(36)) == 36
    assert '=' not in (create_base_list(36))
    with pytest.raises(ValueError):
        create_base_list(37)
    with pytest.raises(ValueError):
        create_base_list(0)
    with pytest.raises(ValueError):
        create_base_list(3.5)


def test_any_to_decimal():
    assert any_to_decimal("1100", 2) == 12
    assert any_to_decimal("aaa", 13) == 1830
    assert any_to_decimal("25", 10) == 25
    assert any_to_decimal("ff", 16) == 255
    assert any_to_decimal("11A", 16) == 282
    with pytest.raises(TypeError):
        any_to_decimal("deadbeef", 10)


def test_convert():
    assert convert("deadbeef", 16, 10) == "3735928559"
    assert convert("10", 10, 10) == "10"
    assert convert("0", 10, 2) == "0"
    assert convert("2", 10, 2) == "10"
    assert convert("11", 10, 16) == "B"
    assert convert("XYZ", 36, 2) == "1010101111111011"

